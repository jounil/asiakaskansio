@echo off
set /p ASIAKAS="Asiakkaan nimi: "
mkdir %ASIAKAS%
cd %ASIAKAS%

set /p PROJEKTI="Asiakkaan projekti: "
mkdir %PROJEKTI%
cd %PROJEKTI%

mkdir assets
mkdir business
mkdir design
mkdir input
mkdir production
